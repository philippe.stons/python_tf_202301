"""
Simuler un distrubuteur de boisson, un utilisateur peut demander une boisson, si il en reste. Le distrubeur donne la
boisson et la retire de son inventaire; sinon le distributeur alerte l'utilisateur qu'il n'y a plus de stock restant.
À tout moment l'utilisateur peut décider de quitter le distributeur.
"""

contenu = [
    { "name": "Coca", "qty": 2 },
    { "name": "Fanta", "qty": 5 },
    { "name": "Sprite", "qty": 3 },
    { "name": "Red bull", "qty": 8 },
]

in_nbr = -1

while in_nbr != 99:
    print("Entrer 99 pour quitter, 100 pour lister l'inventaire ou l'un des numéro d'une boisson :")

    for i, drink in enumerate(contenu):
        # je vérifie qu'il y a encore du stock de cette boisson avant de l'imprimer.
        if drink["qty"] > 0:
            print(i, drink["name"])

    in_nbr = int(input("Choix : "))

    # je vérifie que le nombre est bien plus petit que 99 (qui sert à quitter le programme)
    # que celui-ci est bien un index valide dans le contenu (entre 0 et len(contenu) - 1)
    # et qu'il reste encore de cette boisson (qty > 0)
    if in_nbr < 99 and in_nbr >= 0 and in_nbr < len(contenu) and contenu[in_nbr]["qty"] > 0:
        contenu[in_nbr]["qty"] -= 1
        print("Voici votre", contenu[in_nbr]["name"])
    # si l'utilisateur entre 100 on liste le nom et la quantité restante de la boisson
    elif in_nbr == 100:
        for drink in contenu:
            print(drink["name"], drink["qty"])
    else:
        print("Erreur, soit la boisson n'existe pas soit il n'y en a plus en stock")
