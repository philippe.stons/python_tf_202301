my_list = []

print(my_list)
my_list.append("Riri")
print(my_list)
my_list.append("Fifi")
my_list.append("Loulou")
print(my_list)
my_list.insert(1, "Daisy")
print(my_list)

my_dict = { "firstname": "Donald" }
print(my_dict)
my_dict["lastname"] = "Duck"
print(my_dict)

my_dict_2 = { "firstname": "Riri", "lastname": "Duck" }
my_dict_2["year_reulst"] = 12
print(my_dict)
print(my_dict_2)

for i in range(10):
    print(i, "Hello!!")

i = 0
while i < 10:
    print(i, "Hello!!")
    i += 1

out_char = "q"
quit_loop = 0
while quit_loop != out_char:
    print("Demander les chiffres et faire le calcul")
    quit_loop = input("Voulez-vous continuer ? si non entrer 'q' : ")
