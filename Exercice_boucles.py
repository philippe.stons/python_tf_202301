"""
À la place d'utiliser print(start) transformer le code pour qu'il stock
les valeur dans une list donc avec les valeur suivante en entrée : 5 25 5
la sortie doit ressembler à
[5, 10, 15, 20, 25]
au lieu de
5
10
15
20
25
"""

start = int(input("Entrer le nombre de départ : "))
max = int(input("Entrer le nombre max : "))
step = int(input("Entrer le pas : "))
start2 = start
nbrs = []

while start <= max:
    # print(start)
    nbrs.append(start)
    start += step
print(nbrs)

# for i in range(start2, max + 1, step):
#     print(i)

