"""
À l’aide d’une boucle, modifier la calculatrice pour demander à l’utilisateur quel calcul il désire faire tant qu’il
désire continuer. (par exemple lui dire appuyer sur “q” pour sortir).
"""
import random

quit = ""

while quit != 'q':
    a = int(input("Entrer le premier nombre : "))

    operator = ""
    # l'opérateur sera une string égale à "+" "-" "/" "*"
    # initialisation de l'opérateur
    # while operator not in ["+", "-", "*", "/"]:
    while operator != "+" and operator != "-" and operator != "*" and operator != "/":
        operator = input("Entrer l'opérateur : ")

    b = int(input("Entrer le second nombre : "))


    if operator == "+":
        print(a + b)
    elif operator == "-":
        print(a - b)
    elif operator == "*":
        print(a * b)
    elif operator == "/":
        if b != 0:
            print(a / b)
        else:
            print("Error division par zéro")
    else:
        print("Unkown operator")

    quit  = input("Tapper 'q' pour quitter : ")

"""
Faire un algorithme qui génère un nombre aléatoire (pour générer un nombre aléatoire entre 0 et 99 il y a la fonction 
suivante random.randint(0, 100) ). Ensuite demander à l’utilisateur en boucle un nombre et afficher lui si le nombre 
entré est “plus grand”, “plus petit” ou si c’est le bon nombre lui afficher qu’il a gagné. Continuer de demander des 
nombres tant que l’utilisateur n’a pas trouvé le bon.
"""

nbr = random.randint(0, 100) # il génère un nombre entre 0 et 99
in_nbr = -1 # donc ici je veux m'assurer d'initialiser in_nbr à un nombre ne pouvant
            # pas être généré par la ligne précédente

print(nbr) #j'imprime le nombre aléatoire

while nbr != in_nbr:
    in_nbr = int(input("Entrer un nombre : "))

    if in_nbr < nbr:
        print("Trop petit")
    elif in_nbr > nbr:
        print("Trop grand")
    else:
        print("Égale!")

"""
Demander à l’utilisateur d’entrer son mot de passe (stocké dans une variable du programme), si il entre le bon mot de 
passe, on le salue, si il n’entre pas le bon mots de passe on lui affiche le nombre d’essais restant (3 essais maximum), 
si il n’a plus d’essais restant, lui dire que le compte est bloqué.
"""

password = "test1234="
in_password = ""
nbr_try = 3

while in_password != password and nbr_try > 0:
    nbr_try -= 1

    in_password = input("Entrer le mots de passe : ")

if in_password == password:
    print("Bienvenue")
elif nbr_try == 0:
    print("Votre compte est bloqué")

"""
Réalisez un algorithme qui demande un nombre à l'utilisateur et affiche autant de ligne que le nombre spécifié par 
l'utilisateur. Exemple : l'utilisateur a rentré le nombre 5 et l'algorithme affiche :

*
**
***
****
*****

"""

nbr_lines = int(input("Entrer un nombre : "))

for i in range(1, nbr_lines + 1):
    print("*" * i)

"""
Ecrivez un algorithme qui demande à l’utilisateur de taper 10 entiers et qui affiche le plus petit de ces entiers.
"""

smallest = int(input("Entrer un nombre : "))

for i in range(9):
    nbr = int(input("Entrer un nombre : "))

    if nbr < smallest:
        smallest = nbr

print(smallest)

nbr_list = []

for i in range(10):
    nbr = int(input("Entrer un nombre : "))
    nbr_list.append(nbr)

print(min(nbr_list))
