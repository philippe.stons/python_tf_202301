a = 0
b = 1
max = int(input("Enter the max"))
fibo = [0 , 1]

while a + b < max:
    tmp = a
    a = b
    b = tmp + b
    fibo.append(b)

print(fibo)